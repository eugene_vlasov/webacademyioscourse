//
//  PhoneClass.swift
//  iosLesson4
//
//  Created by Vlasov, Eugene on 02.09.2021.
//

import UIKit

enum ContactList {
    case friend,mom,boss
}

class PhoneClass {
    let brandName : String
    let model : String
    let color : UIColor
    let contacList : ContactList
    
    init(brandName : String, model : String, color : UIColor ) {
        self.brandName = brandName
        self.model = model
        self.color = color
    
    }
    
    func call() {
        print("You are on the call")
    }
    
    func photo() {
        print("You took a new photo")
    }
    
    func textMom() {
        print("You texted a message to your \(.mom)")
    }
    
    func addContact(name : String){
        contacList.case.name
    }
    
    func checkContactList () {
        print(contacList)
    }
    
}
