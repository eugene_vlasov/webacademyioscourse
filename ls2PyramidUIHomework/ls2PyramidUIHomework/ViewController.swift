//
//  ViewController.swift
//  iosLesson2
//
//  Created by Vlasov, Eugene on 26.08.2021.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //----------------------------------------
        //Первое задание про пирамиды
        // pyramidUIViewFirstTask(amountOfBoxes: 5) //Ввести кол-во отображаемых ящиков
        //----------------------------------------
        //Второе задание про пирамиды
        pyramidUIViewSecondTask(pyramidLevel: 4) //Ввести уровень высоты пирамиды
        //----------------------------------------
        //Третье задание про пирамиды
        //pyramidUIViewThirdTask(pyramidLevel: 0) //Ввести уровень высоты пирамиды
        //----------------------------------------
    }
    
    
    
    func addBox(x: Int, y: Int){
        let boxView = UIView()
        
        boxView.frame = CGRect(x: x, y: y, width: 50, height: 50)
        boxView.backgroundColor = .cyan
        
        view.addSubview(boxView)
    }
    
    func pyramidUIViewFirstTask(amountOfBoxes : Int) {
        if amountOfBoxes == 0 {
            return
        } else {
            var x : Int = 30
            let y : Int = 130
            for _ in 1...amountOfBoxes {
                addBox(x: x, y: y)
                x+=60
            }
        }
    }
    
    func pyramidUIViewSecondTask(pyramidLevel : Int) {
        if pyramidLevel == 0 {
            return
        } else {
            var amountOfBoxes = 1
            var x : Int = 30
            var y : Int = 130
            
            for _ in 1...pyramidLevel{
                for _ in 1...amountOfBoxes {
                    addBox(x: x, y: y)
                    x+=60
                    
                }
                amountOfBoxes+=1
                x = 30
                y += 60
                
            }
        }
        
    }
    
    func pyramidUIViewThirdTask(pyramidLevel : Int) {
        if pyramidLevel == 0 {
            return
        } else {
            var amountOfBoxes = 1
            var x : Int = 170
            var y : Int = 250
            var startX : Int = 170
            
            
            for _ in 1...pyramidLevel{
                for _ in 1...amountOfBoxes {
                    addBox(x: x, y: y)
                    x+=60
                    
                }
                amountOfBoxes+=1
                x = startX - 30
                startX = x
                y+=60
                
            }
        }
        
    }
}

