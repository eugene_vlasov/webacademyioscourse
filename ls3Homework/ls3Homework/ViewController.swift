//
//  ViewController.swift
//  ls3Homework
//
//  Created by Vlasov, Eugene on 02.09.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       // printRowAndReverse(x: 5)
       // countDelimeters(a: 6)
        //perfectNumber(a: 28)
       // sellManhettenTask1()
        reverseNumber(x: 342)
    }

    //-------------------------------------------------
    // Assignment #2. Block 1.
    func printMax (a : Int, b : Int){
        if a > b {
            print(a)
        } else if b > a {
            print(b)
        } else {
            print("a = b")
        }
    }
    
    func printExp(a : Int) {
        print(a*a)
        print(a*a*a)
    }
    
    func printRowAndReverse(x : Int) {
        for i in 0...x {
            print(i, x-i)
        }
    
    }
    
    
    func countDelimeters(a : Int) {
        var count : Int = 0
        for i in 1...a {
            if a%i == 0 {
                print(i)
                count+=1
            }
        }
        print(count)
    }
    
    func perfectNumber (a : Int) {
        var sum : Int = 0
        for i in 1...a {
            if a%i == 0 {
                sum+=i
                if a != i {
                    print(i)
                }
            }
        }
        if (sum - a) == a {
            print("perfecr number")
        } else {
            print("not perfect number")
        }
    }
    
    //-------------------------------------------------
    // Assignment #2. Block 2
    
    func sellManhettenTask1() {
        let firstYear : Int = 1826
        let currentYear : Int = 2021
        var Price : Double = 24
        var newPrice : Double
        let yearPercent : Double = 0.06
        
        for _ in firstYear...currentYear {
            newPrice = Price * yearPercent
            Price +=  newPrice
            
        }
        print(Price)
        
        //let result = startPrice * pow((1 + yearPercent/100), years)
       // print(result)
    }
    
    func reverseNumber (x : Int) {
        let temp : String = String(x)
        print(temp.reversed())
        
    }


}

